package com.hcl.devops;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DevopsgradedApplication {

	public static void main(String[] args) {
		SpringApplication.run(DevopsgradedApplication.class, args);
	}

}
